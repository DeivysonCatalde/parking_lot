import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:parkinglot/src/app/app_widget.dart';
import 'package:parkinglot/src/shared/colors.dart';


void main() {

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: AppColors.primary, // status bar color
  ));
  runApp(AppWidget());
}

