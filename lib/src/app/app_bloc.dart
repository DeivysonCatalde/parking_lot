import 'dart:convert';

import 'package:bloc_provider/bloc_provider.dart';
import 'package:parkinglot/src/shared/models/car.dart';
import 'package:parkinglot/src/shared/models/operation.dart';
import 'package:parkinglot/src/shared/models/vacancy.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppBloc extends Bloc {
  var _indexBarController = BehaviorSubject<int>();

  Stream<int> get outIndex => _indexBarController.stream;

  get setIndex => _indexBarController.sink.add;

  var spotController = BehaviorSubject<List<ParkingSpot>>();

  Stream<List<ParkingSpot>> get outspots => spotController.stream;

  var historicController = BehaviorSubject<List<Operation>>();

  Stream<List<Operation>> get outHistoric => historicController.stream;

  getSpots() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? list = (prefs.getStringList("spots"));
    List<ParkingSpot> spots = [];

    if (list == null) {
      for (int i = 0; i < 10; i++) {
        spots.add(ParkingSpot(number: (i + 1), vacancy: true));
      }
    } else {
      list.forEach((element) {
        spots.add(ParkingSpot.fromMap(jsonDecode(element)));
      });
    }
    spotController.sink.add(spots);
  }

  getOperations() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? list = (prefs.getStringList("historic"));
    List<Operation> ops = [];

    if (list == null) {
    } else {
      list.forEach((element) {
        ops.add(Operation.fromMap(jsonDecode(element)));
      });
    }
    historicController.sink.add(ops);
  }

  checkIn(int index, Car car) {
    var list = spotController.value;
    list[index].car = car;
    list[index].vacancy = false;
    list[index].dateIn = DateTime.now();

    spotController.sink.add(list);
    saveList();
    Operation op = Operation("Entrada", DateTime.now(), list[index]);
    addOperation(op);
    getOperations();
  }

  checkOut(int index) async {
    var list = spotController.value;
    Operation op = Operation("Saída", DateTime.now(), list[index]);
    await addOperation(op);
    await getOperations();
    list[index].car = null;
    list[index].vacancy = true;
    list[index].dateOut = DateTime.now();

    spotController.sink.add(list);
    saveList();
  }

  cleanHistoic() async {
    var prefs = await SharedPreferences.getInstance();

    prefs.setStringList("historic", []);
    historicController.sink.add([]);
  }

  saveList() async {
    List<ParkingSpot> list = spotController.value;
    List<String> listTosave = [];
    list.forEach((element) {
      listTosave.add(jsonEncode(element.tomap()));
    });
    SharedPreferences.getInstance().then((value) {
      value.setStringList("spots", listTosave);
    });
  }

  addOperation(Operation operation) async {
    var prefs = await SharedPreferences.getInstance();
    List<String>? listHistoric = prefs.getStringList("historic");
    if (listHistoric == null) {
      listHistoric = [];
    }

    listHistoric.add(jsonEncode(operation.toMap()));
    prefs.setStringList("historic", listHistoric);
  }

  @override
  void dispose() {
    _indexBarController.close();
    spotController.close();
    historicController.close();
    // TODO: implement dispose
  }
}
