import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/features/screens/home_page.dart';
import 'package:parkinglot/src/shared/colors.dart';


import 'app_bloc.dart';
class AppWidget extends StatefulWidget {


  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppBloc>(
      creator: (_context, _bag) => AppBloc(),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Controle estacionamento',
          theme: ThemeData(
            primaryColor: AppColors.primary,
            primarySwatch: Colors.blue,
          ),
          //home: TrackingModule(),
         home: HomePage(),
      ),
    );
  }
}