import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/app/app_bloc.dart';
import 'package:parkinglot/src/shared/models/car.dart';

class Entrance extends StatefulWidget {
  int numberVacancy;

  Entrance(this.numberVacancy);

  @override
  _EntranceState createState() => _EntranceState();
}

class _EntranceState extends State<Entrance> {
  var key = GlobalKey<FormState>();
  var idController = TextEditingController();
  var ownerController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dados do veículo"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Form(
          key: key,
          child: ListView(
            children: [
              TextFormField(
                controller: idController,
                validator: (text) {
                  if (text!.isEmpty) return "Campo obrigatório";
                },
                decoration: InputDecoration(
                  labelText: "Placa",
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: ownerController,
                validator: (text) {
                  if (text!.isEmpty) return "Campo obrigatório";
                },
                decoration: InputDecoration(
                  labelText: "Propietário",
                ),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (key.currentState!.validate()) {
                      Car car = Car(
                          id: idController.text,
                          ownerName: ownerController.text);
                      BlocProvider.of<AppBloc>(context)
                          .checkIn(widget.numberVacancy - 1, car);
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text("Confirmar Entrada"))
            ],
          ),
        ),
      ),
    );
  }
}
