import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/app/app_bloc.dart';
import 'package:parkinglot/src/shared/colors.dart';
import 'package:parkinglot/src/shared/models/operation.dart';
import 'package:parkinglot/src/shared/utils/format_date.dart';

class HistoricPage extends StatefulWidget {
  @override
  _HistoricPageState createState() => _HistoricPageState();
}

class _HistoricPageState extends State<HistoricPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Histórico"),
        actions: [
          TextButton(
              onPressed: () {
                BlocProvider.of<AppBloc>(context).cleanHistoic();
              },
              child: Text(
                "Limpar",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: StreamBuilder<List<Operation>>(
          initialData: [],
          stream: BlocProvider.of<AppBloc>(context).outHistoric,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  return operation(snapshot.data![index]);
                },
              );
            }
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(AppColors.primary),
            ));
          }),
    );
  }

  operation(Operation op) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                        width: 70,
                        child: Text("${op.operation.toString()}",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600))),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(op.operation == "Entrada" ? Icons.login : Icons.logout)
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Vaga: ",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      op.spot!.number.toString(),
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                Text("Placa do veículo: ${op.spot!.car!.id}"),
                Text("Propietário: ${op.spot!.car!.ownerName}"),
                Text("Data: ${formatDate(op.date!)} - ${formatHour(op.date!)}"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
