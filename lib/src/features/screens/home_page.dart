import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/app/app_bloc.dart';
import 'package:parkinglot/src/features/screens/historic_page.dart';
import 'package:parkinglot/src/features/screens/parked_cars_page.dart';
import 'package:parkinglot/src/features/screens/spot_page.dart';
import 'package:parkinglot/src/shared/colors.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var index = 0;

  var pageController = PageController(initialPage: 0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<AppBloc>(context).getSpots();
    BlocProvider.of<AppBloc>(context).getOperations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: StreamBuilder<int>(
          initialData: 0,
          stream: BlocProvider.of<AppBloc>(context).outIndex,
          builder: (context, snapshot) {
            return BottomNavigationBar(
              currentIndex: snapshot.data ?? 0,
              backgroundColor: AppColors.primary,
              selectedItemColor: Colors.white,
              onTap: (index) {
                BlocProvider.of<AppBloc>(context).setIndex(index);
                pageController.animateToPage(
                  index,
                  curve: Curves.ease,
                  duration: Duration(seconds: 1),
                );
              },
              items: [
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.place,
                    ),
                    label: "Vagas"),
                BottomNavigationBarItem(
                    icon: Icon(Icons.local_shipping), label: "Estacionados"),
                BottomNavigationBarItem(
                    icon: Icon(Icons.history), label: "Histórico")
              ],
            );
          }),
      body: PageView(
        controller: pageController,
        onPageChanged: (index) {
          BlocProvider.of<AppBloc>(context).setIndex(index);
        },
        children: [
          SpotPage(),
          ParkedCarsPage(),
          HistoricPage(),
        ],
      ),
    );
  }
}
