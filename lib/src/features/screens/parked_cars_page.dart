import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/app/app_bloc.dart';
import 'package:parkinglot/src/shared/colors.dart';
import 'package:parkinglot/src/shared/models/vacancy.dart';
import 'package:parkinglot/src/shared/utils/format_date.dart';

class ParkedCarsPage extends StatefulWidget {
  @override
  _ParkedCarsPageState createState() => _ParkedCarsPageState();
}

class _ParkedCarsPageState extends State<ParkedCarsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vagas Ocupadas"),
      ),
      body: StreamBuilder<List<ParkingSpot>>(
          initialData: null,
          stream: BlocProvider.of<AppBloc>(context).outspots,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  return !snapshot.data![index].vacancy
                      ? parked(snapshot.data![index])
                      : Container();
                },
              );
            }
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(AppColors.primary),
            ));
          }),
    );
  }

  parked(ParkingSpot spot) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Vaga: ",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      spot.number.toString(),
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                Text("Placa do veículo: ${spot.car!.id}"),
                Text("Propietário: ${spot.car!.ownerName}"),
              ],
            ),
            ElevatedButton(
                onPressed: () {
                  dialogCheckOou(context, spot);
                },
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.red))),
                    backgroundColor: MaterialStateProperty.all(Colors.red)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Registrar\nsaída",
                    textAlign: TextAlign.center,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  dialogCheckOou(context, ParkingSpot spoted) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          title: Text("Confirmar a Saída "),
          content: Text(
              "Deseja confirmar a saída do veículo de placa: ${spoted.car!.id}\n"
              "tempo de permanência: ${(DateTime.now().difference(spoted.dateIn!).inMinutes)} minutos"
              ""),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new TextButton(
                  child: new Text(
                    "Cancelar",
                    style: TextStyle(color: Colors.red),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new TextButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    BlocProvider.of<AppBloc>(context)
                        .checkOut((spoted.number! - 1));
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )
          ],
        );
      },
    );
  }
}
