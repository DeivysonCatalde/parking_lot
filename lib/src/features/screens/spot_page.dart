import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:parkinglot/src/app/app_bloc.dart';
import 'package:parkinglot/src/features/screens/entrance.dart';
import 'package:parkinglot/src/shared/colors.dart';
import 'package:parkinglot/src/shared/models/vacancy.dart';
import 'package:parkinglot/src/shared/utils/nav.dart';

class SpotPage extends StatefulWidget {
  @override
  _SpotPageState createState() => _SpotPageState();
}

class _SpotPageState extends State<SpotPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vagas disponíveis"),
      ),
      body: StreamBuilder<List<ParkingSpot>>(
          initialData: null,
          stream: BlocProvider.of<AppBloc>(context).outspots,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  return snapshot.data![index].vacancy
                      ? spot(snapshot.data![index])
                      : Container();
                },
              );
            }
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(AppColors.primary),
            ));
          }),
    );
  }

  spot(ParkingSpot spot) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Text(
                  "Vaga: ",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                Text(
                  spot.number.toString(),
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            ElevatedButton(
                onPressed: () {
                  goTo(context, Entrance(spot.number!));
                },
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
                    backgroundColor: MaterialStateProperty.all(Colors.green)),
                child: Text("Entrada"))
          ],
        ),
      ),
    );
  }
}
