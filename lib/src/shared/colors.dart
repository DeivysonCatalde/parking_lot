import 'package:flutter/material.dart';

class AppColors {
  static final primary= _hexToColor("#475791");
  static final green= _hexToColor("#00934C");
  static final lightBlue = _hexToColor("#618ED0");
  static final red = _hexToColor("#990000");
  static final orange = _hexToColor("#F2994A");



  static Color _hexToColor(String hexColor) {
    String newColor = '0xff' + hexColor.replaceAll("#", "");
    int hexColorInFlutter = int.parse(newColor);
    return Color(hexColorInFlutter);
  }
  static Color hexToColor(String hexColor) {
    String newColor = '0xff' + hexColor.replaceAll("#", "");
    int hexColorInFlutter = int.parse(newColor);
    return Color(hexColorInFlutter);
  }

  static String hexOfRGBA(int r,int g,int b,{double opacity=1})
  {
    r = (r<0)?-r:r;
    g = (g<0)?-g:g;
    b = (b<0)?-b:b;
    opacity = (opacity<0)?-opacity:opacity;
    opacity = (opacity>1)?255:opacity*255;
    r = (r>255)?255:r;
    g = (g>255)?255:g;
    b = (b>255)?255:b;
    return ('${r.toRadixString(16).padLeft(2,"0")}${g.toRadixString(16).padLeft(2,"0")}${b.toRadixString(16).padLeft(2,"0")}').toString();
  }

}