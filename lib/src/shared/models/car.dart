class Car{
  String? id;
  String? ownerName;


  Car({this.id, this.ownerName});

  Car.fromMap(Map map){
    this.id = map['id'];
    this.ownerName = map["ownerName"];


  }
  Map toMap(){
    return {
      "id":this.id,
      "ownerName":this.ownerName
    };
  }

}