import 'package:parkinglot/src/shared/models/vacancy.dart';

class Operation{

  String? operation;
  DateTime? date;
  ParkingSpot? spot;

  Operation(this.operation, this.date, this.spot);


  Operation.fromMap(Map map){
    this.operation = map['operation'];
    this.date = DateTime.parse(map['date']);
    this.spot = ParkingSpot.fromMap(map['spot']);
  }

  Map toMap(){

    return{
      "operation":this.operation,
      "date":this.date!=null?this.date.toString():null,
      "spot": spot!.tomap()
    };
  }


}