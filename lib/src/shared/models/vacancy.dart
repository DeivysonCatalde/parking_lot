import 'package:parkinglot/src/shared/models/car.dart';

class ParkingSpot{

  int?  number ;
  bool vacancy = true;
  Car?car;
  DateTime? dateIn;
  DateTime? dateOut;

  ParkingSpot({this.number, this.vacancy = true, this.car, this.dateIn, this.dateOut});

  ParkingSpot.fromMap(Map map){

    this.number = map['number'];
    this.vacancy = map['vacancy']??true;
    this.car = map["car"]!=null?Car.fromMap(map["car"]):null;
    this.dateIn = map['dateIn']!=null?DateTime.parse(map['dateIn']):null;
    this.dateOut = map['dateOut']!=null?DateTime.parse(map['dateOut']):null;

}
 tomap(){
 return {
   "number":this.number,
   "vacancy": this.vacancy,
   "car": this.car?.toMap(),
   'dateIn':dateIn!=null?this.dateIn.toString():null,
   "dateOut":dateOut!=null?this.dateOut.toString():null


 };
}

}

