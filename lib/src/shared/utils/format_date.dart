
formatDate(DateTime data){
  return "${data.day.toString().padLeft(2,"0")}/${data.month.toString().padLeft(2,"0")}/${data.year.toString().padLeft(2,"0")}";

}
formatHour(DateTime data){
  return "${data.hour.toString().padLeft(2,"0")}:${data.minute.toString().padLeft(2,"0")}";

}